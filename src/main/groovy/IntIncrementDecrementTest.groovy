import groovy.transform.CompileStatic

@CompileStatic
class IntIncrementDecrementTest {
  public static void main(String[] args) {

    def i = 0
    def j = 0

    int x = i++
    int y = ++j
  }
}
