Run `gradle build` to compile. It will produce this error:

```
IntIncrementDecrementTest.groovy: 10: [Static type checking] - Cannot assign value of type java.lang.Object to variable of type int
 @ line 10, column 14.
       int x = i++
                ^

IntIncrementDecrementTest.groovy: 11: [Static type checking] - Cannot assign value of type java.lang.Object to variable of type int
 @ line 11, column 13.
       int y = ++j
               ^

2 errors
```
